//Lấy dữ liệu danh sách món ăn từ mockAPI
const URL_FOOD = `https://63b2c9a15e490925c521a610.mockapi.io/`
function fetchFoodList() {
    onLoading()
    axios({
        url: `${URL_FOOD}food`,
        method: "GET"
    })
        .then(function (res) {
            // console.log(res.data)
            offLoading()
            renderFood(res.data)
        })
        .catch(function (err) {
            console.log(err)
        })
}
fetchFoodList()

//Render ra danh sách món ăn
function renderFood(danhSachMonAn) {
    var totalContent = ``
    danhSachMonAn.reverse().forEach(function (item) {
        var content = `<tr>
                        <td>${item.maMon}</td>
                        <td>${item.tenMon}</td>
                        <td>${item.giaMon}</td>
                        <td>${item.loaiMon
                ? "<span class='text-primary'>Mặn</span>"
                : "<span class='text-success'>Chay</span>"}
                        </td>
                        <td>${item.hinhAnh}</td>
                        <td>
                            <button class="btn btn-danger" onclick="deleteFood('${item.maMon}')">Xóa</button>
                            <button class="btn btn-warning" onclick="editFood('${item.maMon}')">Sửa</button>
                        </td>
                    </tr>`
        totalContent += content
    });
    document.getElementById("tbodyFood").innerHTML = totalContent
}

//Xóa món ăn
function deleteFood(id) {
    onLoading()
    axios({
        url: `${URL_FOOD}food/${id}`,
        method: "DELETE"
    })
        .then(function (res) {
            console.log("🚀 ~ file: main.js:47 ~ .then ~ res", res)
            // renderFood(res.data) không chạy lại hàm render do nó sẽ trả về res.data của 1 id
            offLoading()
            fetchFoodList() //gọi hàm fetchFoodList() để trả về render toàn bộ danh sách
        })
        .catch(function (err) {
            console.log("🚀 ~ file: main.js:50 ~ deleteFood ~ err", err)

        })
}

//Thêm món ăn
function addFood() {
    var food = getData()
    onLoading()
    axios({
        url: `${URL_FOOD}/food`,
        method: "POST",
        data: food
    })
        .then(function (res) {
            console.log("🚀 ~ file: main.js:47 ~ .then ~ res", res)
            offLoading()
            eraseData()
            fetchFoodList()
        })
        .catch(function (err) {
            console.log("🚀 ~ file: main.js:50 ~ deleteFood ~ err", err)

        })
}

//Sửa món ăn
function editFood(id) {
    var food = null
    onLoading()
    axios({
        url: `${URL_FOOD}/food/${id}`,
        method: "GET"
    })
        .then(function (res) {
            console.log("🚀 ~ file: main.js:47 ~ .then ~ res", res)
            offLoading()
            fetchFoodList()
            food = res.data
            postData(food)
        })
        .catch(function (err) {
            console.log("🚀 ~ file: main.js:50 ~ deleteFood ~ err", err)

        })
    document.getElementById("maMon").disabled = true;
}

//Cập nhật món ăn
function updateFood() {
    var food = getData()
    onLoading()
    axios({
        url: `${URL_FOOD}/food/${food.maMon}`,
        method: "PUT",
        data: food
    })
        .then(function (res) {
            offLoading()
            eraseData()
            document.getElementById("maMon").disabled = false;
            fetchFoodList()
        })
        .catch(function (err) {
            console.log("🚀 ~ file: main.js:119 ~ updateFood ~ err", err)
        })
}