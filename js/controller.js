//Bật tắt khi loading
function onLoading() {
    document.getElementById("spinner").style.display = "flex"
}

function offLoading() {
    document.getElementById("spinner").style.display = "none"
}

//Lấy dữ liệu
function getData() {
    var maMon = document.getElementById("maMon").value;
    var tenMon = document.getElementById("tenMon").value;
    var giaMon = document.getElementById("giaMon").value;
    var hinhAnh = document.getElementById("hinhAnh").value;
    var loaiMon = (document.getElementById("loaiMon").value == "Mặn") ? true : false;

    var food = new foods(
        maMon,
        tenMon,
        giaMon,
        hinhAnh,
        loaiMon,
    );
    return food
}

//Xuất dữ liệu
function postData(item) {
    document.getElementById("maMon").value = item.maMon
    document.getElementById("tenMon").value = item.tenMon
    document.getElementById("giaMon").value = item.giaMon
    document.getElementById("hinhAnh").value = item.hinhAnh
    document.getElementById("loaiMon").value = item.loaiMon ? "Mặn" : "Chay"
}

function eraseData() {
    document.getElementById("maMon").value = ""
    document.getElementById("tenMon").value = ""
    document.getElementById("giaMon").value = ""
    document.getElementById("hinhAnh").value = ""
    document.getElementById("loaiMon").value = ""
}